﻿/*
 * Program ID - Final Project
 * 
 * Purpose - Action Scene of Game (playing the game)
 * 
 * Revision History
 *  Written by Daniel Magee, Nov + Dec 2018
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DMageeFinalProject
{
    /// <summary>
    /// Action Scene class, inherits from GameScene
    /// </summary>
    public class ActionScene : GameScene
    {
        //Properties of the Class
        private SpriteBatch spriteBatch;
        private SpriteFont spriteFont;
        private List<Duck> ducks;
        private Aim aim;
        private Dog dog;

        private Texture2D duckTex;
        private Texture2D dogTex;
        private Texture2D aimTex;
        private Vector2 stage;
        private Vector2 duckInitPos;
        private Vector2 duckSpeed;
        private SoundEffect lostDuck;
        private SoundEffect hitDuck;
        private Score score;
        private Lives lives;
        public static Random r = new Random();
        private float timer;

        Vector2 dogPos = new Vector2(30,
            GraphicsDeviceManager.DefaultBackBufferHeight - 255);

        /// <summary>
        /// Constructor of the class
        /// Sets up images/sounds
        /// and necessary class objects to play the game
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        public ActionScene(Game game,
            SpriteBatch spriteBatch) : base(game)
        {
            this.spriteBatch = spriteBatch;

            //sets up necessary media/images and parameters of a Duck object
            //as well as inialize a list to hold multiple duck objects
            duckTex = game.Content.Load<Texture2D>("Images/bird");
            ducks = new List<Duck>();
            lostDuck = game.Content.Load<SoundEffect>("Sounds/wrong");
            hitDuck = game.Content.Load<SoundEffect>("Sounds/deadBird");

            //adds dog animation sprite sheet to game components
            //set parameters of the new Dog object
            dogTex = game.Content.Load<Texture2D>("Images/dogAnimation");
            int delay = 30;
            dog = new Dog(game, spriteBatch, dogTex, dogPos, delay);
            this.Components.Add(dog);

            //sets up the Score object and font
            spriteFont = game.Content.Load<SpriteFont>("Fonts/regularfont");
            score = new Score(game, spriteBatch, spriteFont);
            this.Components.Add(score);

            //sets up the Lives object 
            lives = new Lives(game, spriteBatch, spriteFont);
            this.Components.Add(lives);

            //sets up the players Aim object reticule
            aimTex = game.Content.Load<Texture2D>("Images/aim");
            Vector2 stage = new Vector2(GraphicsDeviceManager.DefaultBackBufferWidth,
               GraphicsDeviceManager.DefaultBackBufferHeight);
            Vector2 aimInitPos = new Vector2(stage.X / 2, stage.Y / 2);
            SoundEffect shot = game.Content.Load<SoundEffect>("Sounds/shot");
            aim = new Aim(game, spriteBatch, aimTex, aimInitPos, stage, shot,score);
            this.Components.Add(aim);

        }

        /// <summary>
        /// Overidden Draw method 
        /// Draws a duck object for every duck found in
        /// the list of Ducks
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            foreach (var duck in ducks)
            {
                duck.Draw(spriteBatch);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Overidden Update method
        /// Runs game logic of updating when
        /// The Lives object isn't 0:
        /// All currently alive Duck objects,
        /// the Aim object
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            //run the Aim object Update method
            aim.Update(gameTime, ducks);

            //Only run when the player still has remaining lives
            if (lives.lives > 0)
            {

                stage = new Vector2(GraphicsDeviceManager.
                    DefaultBackBufferWidth,
                  GraphicsDeviceManager.DefaultBackBufferHeight);

                //randomize where a new duck starts
                duckInitPos = new Vector2(r.Next((int)stage.X - 100,
                    (int)stage.X), r.Next(0, (int)stage.Y / 2));
                //randomize the ducks speed
                duckSpeed = new Vector2(r.Next(-15, -3), 0);


                timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

                //Run the duck update method for every current duck
                foreach (var duck in ducks)
                {
                    duck.Update(gameTime, ducks);

                }

                //add a new duck to the game every few seconds
                if (timer > 1.2f)
                {
                    timer = 0;
                    ducks.Add(new Duck(Game, spriteBatch, duckTex,
                        duckInitPos, duckSpeed, stage,
                        lostDuck, hitDuck, lives));
                }

                //remove a duck from the game when shot
                //or hits the left wall (i.e isRemoved property = true)
                for (int i = 0; i < ducks.Count; i++)
                {
                    Duck d1 = ducks[i];
                    if (d1.isRemoved)
                    {
                        ducks.RemoveAt(i);
                        dog.Enabled = true;
                        dog.Visible = true;
                        i--;
                    }
                }
            }
            else
                Game1.gameOver = true;

            base.Update(gameTime);

        }
    }
}
