﻿/*
 * Program ID - Final Project
 * 
 * Purpose - Aim object (shooting reticule)
 * 
 * Revision History
 *  Written by Daniel Magee, Nov + Dec 2018
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DMageeFinalProject
{
    /// <summary>
    /// Class inheriting from DrawableGameComponent
    /// </summary>
    public class Aim : DrawableGameComponent
    {
        //properties of the class
        private SpriteBatch spriteBatch;
        private Texture2D tex;
        public Vector2 position;
        private Vector2 stage;
        private SoundEffect shoot;
        private Score score;
        Rectangle aimRec;

        /// <summary>
        /// constructor of the class
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="tex"></param>
        /// <param name="position"></param>
        /// <param name="stage"></param>
        /// <param name="shoot"></param>
        /// <param name="score"></param>
        public Aim(Game game,
            SpriteBatch spriteBatch,
            Texture2D tex,
            Vector2 position,
            Vector2 stage,
            SoundEffect shoot,
            Score score) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.tex = tex;
            this.position = position;
            this.stage = stage;
            this.shoot = shoot;
            this.score = score;
        }

        /// <summary>
        /// Overridden Draw method
        /// Draws the aim reticule
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(tex, position, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Update logic that will be called in the Action Scene class
        /// Allows for passing a list of Duck objects
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="ducks"></param>
        public virtual void Update(GameTime gameTime, List<Duck> ducks)
        {
            MouseState ms = Mouse.GetState();
            position = new Vector2(ms.X, ms.Y);
            aimRec = new Rectangle((int)position.X, (int)position.Y,
                tex.Width, tex.Height);

            //checks for collision between the Aim object and a Duck object
            foreach (var duck in ducks)
            {
                Rectangle duckRec;

                duckRec = duck.getBounds();
                if (aimRec.Intersects(duckRec) &&
                    ms.LeftButton == ButtonState.Pressed)
                {
                    duck.isRemoved = true;
                    score.score++;
                }
            }

            //play the shooting sound when left mouse is clicked
            if (ms.LeftButton == ButtonState.Pressed)
            {
                shoot.Play(volume: 0.1f, pitch: 0f, pan: 0f);
            }
            base.Update(gameTime);
        }
    }
}
