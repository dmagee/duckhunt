﻿/*
 * Program ID - Final Project
 * 
 * Purpose - Dog object (animation)
 * 
 * Revision History
 *  Written by Daniel Magee,  Dec 2018
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DMageeFinalProject
{
    /// <summary>
    /// Class for Creating a animated Dog object
    /// </summary>
    public class Dog : DrawableGameComponent
    {
        //properties of the class
        private SpriteBatch spriteBatch;
        private Texture2D tex;
        private Vector2 position;
        private Vector2 dimension;
        private List<Rectangle> frames;
        private int frameIndex = -1;
        private int delay;
        private int delayCounter;

        /// <summary>
        /// Constructor of the class
        ///
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="tex"></param>
        /// <param name="position"></param>
        /// <param name="delay"></param>
        public Dog(Game game,
            SpriteBatch spriteBatch,
            Texture2D tex,
            Vector2 position,
            int delay) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.tex = tex;
            this.position = position;
            this.delay = delay;
            //size of each frame in the sprite sheet
            dimension = new Vector2(63, 55);

            this.Enabled = false;
            this.Visible = false;

            createFrames();
        }

        /// <summary>
        /// Overridden Draw method
        /// Draws the necessary frame of the sprite sheet
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            if (frameIndex >= 0)
            {
                spriteBatch.Begin();
                spriteBatch.Draw(tex, position, frames[frameIndex], Color.White);
                spriteBatch.End();
            }
            base.Draw(gameTime);
        }

        /// <summary>
        /// Overridden Update method
        /// Updates target frame depending on the set delay
        /// Only iterate through first 6 frames
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            delayCounter++;
            if (delayCounter > delay)
            {
                frameIndex++;
                position.X += 100;
                if (frameIndex > 5)
                {
                    frameIndex = -1;
                    position.X = 30;
                    this.Enabled = false;
                    this.Visible = false;
                }
                delayCounter = 0;
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// Creates the dimensions of each of the
        /// 6 frames being used
        /// </summary>
        private void createFrames()
        {
            frames = new List<Rectangle>();
            for (int i = 0; i < 6; i++)
            {
                int x = i * (int)dimension.X;
                int y = 1 * (int)dimension.Y;
                Rectangle r = new Rectangle(x, y, (int)dimension.X,
                    (int)dimension.Y);
                frames.Add(r);

            }
        }
    }
}