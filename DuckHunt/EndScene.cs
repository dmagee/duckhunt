﻿/*
 * Program ID - Final Project
 * 
 * Purpose - Duck Object 
 * 
 * Revision History
 *  Written by Daniel Magee, Nov + Dec 2018
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DMageeFinalProject
{
    /// <summary>
    /// End Scene class, inherits from GameScene
    /// </summary>
    public class EndScene : GameScene
    {
        //properties of the class
        private SpriteBatch spriteBatch;
        private SpriteFont spriteFont;
        private Texture2D tex;
        private Rectangle rec;

        /// <summary>
        /// Constructor of the class
        /// load in image and font
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        public EndScene(Game game,
            SpriteBatch spriteBatch) : base(game)
        {
            this.spriteBatch = spriteBatch;
            tex = game.Content.Load<Texture2D>("Images/gameOver");
            rec= new Rectangle(0, 0, GraphicsDeviceManager.DefaultBackBufferWidth, GraphicsDeviceManager.DefaultBackBufferHeight);
            spriteFont = game.Content.Load<SpriteFont>("Fonts/regularfont");

        }

        /// <summary>
        /// Overridden Draw method
        /// Draw the image and font for EndScene
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(tex,Vector2.Zero,rec,Color.White);
            spriteBatch.DrawString(spriteFont,
                "Game Over: Press Esc key to return to the main menu " , new Vector2(10, Shared.stage.Y - 100), Color.Red);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
