﻿/*
 * Program ID - Final Project
 * 
 * Purpose - To keep track of the players score
 * 
 * Revision History
 *  Written by Daniel Magee, Nov + Dec 2018
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DMageeFinalProject
{
    /// <summary>
    /// A Class inheriting from DrawableGameComponent
    /// </summary>
    public class Score : DrawableGameComponent
    {
        public int score;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;

        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="spriteFont"></param>
        public Score(Game game, SpriteBatch spriteBatch,
            SpriteFont spriteFont) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.spriteFont = spriteFont;
            score = 0;
        }

        /// <summary>
        /// Overridden Draw method
        /// Draws the current score to the screen
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(spriteFont,
                "Score: "+ score.ToString(), new Vector2(10, Shared.stage.Y-100), Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}

