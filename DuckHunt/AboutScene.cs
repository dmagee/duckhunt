﻿/*
 * Program ID - Final Project
 * 
 * Purpose - About Scene of Game
 * 
 * Revision History
 *  Written by Daniel Magee, Dec 2018
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DMageeFinalProject
{
    /// <summary>
    /// About Scene class, inherits from GameScene
    /// 
    /// </summary>
    public class AboutScene : GameScene
    {
        //Properties of the AboutScene class
        private SpriteBatch spriteBatch;
        private Texture2D tex;
        private Rectangle rec;

        /// <summary>
        /// Constructor of the class
        /// Sets up image being used for scene
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        public AboutScene(Game game,
            SpriteBatch spriteBatch) : base(game)
        {
            this.spriteBatch = spriteBatch;
            tex = game.Content.Load<Texture2D>("Images/aboutScene");
            rec = new Rectangle(-200, -20,
                GraphicsDeviceManager.DefaultBackBufferWidth,
                GraphicsDeviceManager.DefaultBackBufferHeight);
        }

        /// <summary>
        /// Overridden Draw method, draws the About scene image to the screen
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(tex, Vector2.Zero, rec, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
