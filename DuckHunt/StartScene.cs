﻿/*
 * Program ID - Final Project
 * 
 * Purpose - Start Scene - Show menu items
 * 
 * Revision History
 *  Written by Daniel Magee, Nov + Dec 2018
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DMageeFinalProject
{
    /// <summary>
    /// A class inherting from GameScene
    /// </summary>
   public class StartScene: GameScene
    {
        // properties of the class
        public MenuComponent Menu { get; set; }

        private SpriteBatch spriteBatch;
        private string[] menuItems =
            {
            "Start Game",
            "Help",
            "About",
            "Quit"
        };

        /// <summary>
        /// Constructor of the class
        /// Use MenuComponent object to create new menu items
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        public StartScene(Game game,
            SpriteBatch spriteBatch) : base(game)
        {
            this.spriteBatch = spriteBatch;
            SpriteFont regularFont =
                game.Content.Load<SpriteFont>("Fonts/regularfont");
            SpriteFont hilightFont =
                game.Content.Load<SpriteFont>("Fonts/hilightfont");
            Menu = new MenuComponent(game,
                spriteBatch, regularFont, hilightFont, menuItems);
            this.Components.Add(Menu);
        }

    }
}
