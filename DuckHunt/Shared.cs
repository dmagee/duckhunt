﻿/*
 * Program ID - Final Project
 * 
 * Purpose - Allow for the stage dimensions to be shared
 * 
 * Revision History
 *  Written by Daniel Magee, Nov + Dec 2018
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DMageeFinalProject
{
    public class Shared
    {
        //property of the class
        public static Vector2 stage;
    }
}
