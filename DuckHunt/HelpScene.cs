﻿/*
 * Program ID - Final Project
 * 
 * Purpose - Help Scene -- Show how to play game
 * 
 * Revision History
 *  Written by Daniel Magee, Nov + Dec 2018
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DMageeFinalProject
{
    /// <summary>
    /// Scene that inherits from GameScene
    /// </summary>
  public class HelpScene: GameScene
    {
        //properties of the class
        private SpriteBatch spriteBatch;
        private Texture2D tex;
        private Rectangle rec;

      /// <summary>
      /// Constructor of the class
      /// </summary>
      /// <param name="game"></param>
      /// <param name="spriteBatch"></param>
        public HelpScene(Game game,
            SpriteBatch spriteBatch) : base(game)
        {
            this.spriteBatch = spriteBatch;
            tex = game.Content.Load<Texture2D>("Images/helpScene");
            rec = new Rectangle(0, 0, GraphicsDeviceManager.DefaultBackBufferWidth, GraphicsDeviceManager.DefaultBackBufferHeight);

        }

        /// <summary>
        /// Overridden Draw method
        /// draw the help scene image
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(tex, Vector2.Zero,rec,Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
