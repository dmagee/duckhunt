﻿/*
 * Program ID - Final Project
 * 
 * Purpose - Lives object. Allow the game to have 
 * a system for losing
 * 
 * Revision History
 *  Written by Daniel Magee, Nov + Dec 2018
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace DMageeFinalProject
{
    /// <summary>
    /// A class inheriting from DrawableGameComponent
    /// </summary>
    public class Lives : DrawableGameComponent
    {
        //properties of the class
        public int lives;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;

        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="spriteFont"></param>
        public Lives(Game game,
            SpriteBatch spriteBatch,
            SpriteFont spriteFont) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.spriteFont = spriteFont;
            lives = 3;
        }

        /// <summary>
        /// Overriddn Draw method
        /// Draw the remaining lives to the screen
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(spriteFont,
                "Lives: " + lives.ToString(), new Vector2(10, Shared.stage.Y - 70), Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
