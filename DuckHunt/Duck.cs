﻿/*
 * Program ID - Final Project
 * 
 * Purpose - Duck Object 
 * 
 * Revision History
 *  Written by Daniel Magee, Nov + Dec 2018
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DMageeFinalProject
{
    /// <summary>
    /// Class for Creating a duck object
    /// A duck will be the "enemy" that must be shot
    ///to score points
    /// </summary>
    public class Duck : DrawableGameComponent
    {
        //properties of the class
        private SpriteBatch spriteBatch;
        private Texture2D tex;
        private Vector2 position;
        private Vector2 speed;
        private Vector2 stage;
        private Vector2 origin;
        private SoundEffect lostDuck;
        private SoundEffect hitDuck;
        public bool isRemoved = false;
        private Lives lives;
        private float rotation;
        private Rectangle srcRect;

        private float rotationChange = 0.03f;

        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="tex"></param>
        /// <param name="position"></param>
        /// <param name="speed"></param>
        /// <param name="stage"></param>
        /// <param name="lostDuck"></param>
        /// <param name="hitDuck"></param>
        /// <param name="lives"></param>
        public Duck(Game game,
            SpriteBatch spriteBatch,
            Texture2D tex,
            Vector2 position,
            Vector2 speed,
            Vector2 stage,
            SoundEffect lostDuck,
            SoundEffect hitDuck,
            Lives lives) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.tex = tex;
            this.position = position;
            this.speed = speed;
            this.stage = stage;
            this.lostDuck=lostDuck; 
            this.hitDuck=hitDuck;
            this.lives = lives;

            origin= new Vector2(tex.Width / 2, tex.Height / 2);
            srcRect = new Rectangle(0, 0, tex.Width / 2, tex.Height);
         
        }

        /// <summary>
        /// a Draw method that will be called inside the ActionScene's
        /// Overriden Draw (didn't override here because i need to draw
        /// all ducks in a List
        /// </summary>
        /// <param name="spriteBatch"></param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(tex, position, srcRect, Color.White, rotation, origin, 1f, SpriteEffects.None, 0f);
        }

        /// <summary>
        /// A Update method that will be called inside the ActionScene's
        /// Overridden Update (needed to pass a list
        /// of ducks so did not overide here)
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="ducks"></param>
        public virtual void Update(GameTime gameTime, List<Duck> ducks)
        {
            position += speed;
            rotation -= rotationChange;

            if (position.X<0)
            {
                isRemoved = true;
                lives.lives--;
                lostDuck.Play();

            }

        }
        /// <summary>
        /// Set the rectangle bounds of a duck 
        /// </summary>
        /// <returns></returns>
        public Rectangle getBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, tex.Width, tex.Height);
        }

    }
}
