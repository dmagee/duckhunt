# README #


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Requirements WINDOWS ONLY: ###
- Visual Studio 2017 or 2019
- [Git](https://git-scm.com/downloads)
- C# .NET Framework 4.5
- [MonoGame](https://www.monogame.net/downloads/)

### Installation: ###
```
cd <to desired directory>
git clone https://dmagee@bitbucket.org/dmagee/duckhunt.git
```
## Create a release build  / one-click-installer: ##

- Ensure the build is set to 'release'
- Go to project properties > Publish
- Click publish wizard, accept defaults or change as desired.
- A publish folder will be created with the one-click-installer. 
- Run the installer to install and play the game

### License: ###

MIT © Daniel Magee

I chose an MIT license because I would like my project to be open-source indie game.


